#!/bin/bash

# TODO: add params to activae functions
# 1. update sys?
# 2. install mesa and opencl?
# 3. install xmr-stak?
#
# TODO: To add: 
#   Plex, 
#   minetest, 
#   xmr-stak, 
#       https://github.com/fireice-uk/xmr-stak-cpu/blob/master/config.txt
#       sudo sysctl -w vm.nr_hugepages=128" and increase your ulimit -l.
#       edit /etc/security/limits.conf 
#   torrent
#   rdiff-backup
#   nagios
#   graphite
#   sensors: https://askubuntu.com/a/15833/596759
#
#   password: openssl rand -base64 32


SQUIDRAMSIZE='512'
SQUIDDISKSIZE='1024'
HAF_IP='192.168.1.131'
#HAF_NETMASK='255.255.255.0'
HAF_NETWORK='192.168.1.0'
HAF_BROADCAST='192.168.1.255'
HAF_GATEWAY='192.168.1.1'
LAP_IP='192.168.1.101'

function install-swapspace {
    sudo apt -y install swapspace
}

function install-fail2ban {
    sudo apt -y install fail2ban
    sudo cat << EOF | sudo tee /etc/fail2ban/jail.local >> /dev/null
[DEFAULT]
ignoreip = 127.0.0.0/8 10.0.0.0/8 172.16.0.0/12 192.168.0.0/16
EOF
}

function setup_squid {
    sudo apt -y install squid

    pushd /etc/squid

    sudo cp -n squid.conf squid.conf.org
    squidtmp="$(mktemp)"
    md5sum squid.conf > "$squidtmp"
    sudo sed -ri "s/^#?(\s?cache_mem\s)[0-9]+(.*)/\1${SQUIDRAMSIZE}\2/" squid.conf
    sudo sed -ri "s|^#?(cache_dir ufs /var/spool/squid )[0-9]+(.*)|\1${SQUIDDISKSIZE}\2|" squid.conf

    if ! md5sum -c "$squidtmp"
    then
        sudo service squid restart
    fi

    rm "$squidtmp"

    popd
}

function customize_bash {
    # Color to command line

    # Root
    sudo sed -ri "s/^#(force_color_prompt=yes)/\1/" /root/.bashrc
    # TODO: How to change prompt color? So many special characters!
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

    # keith, or ubuntu if in Vagrant
    sed -ri "s/^#(force_color_prompt=yes)/\1/" "/home/${USER}/.bashrc"

    source ~/.bashrc
}

function setup-ssh {
    #sudo mkdir -p /root/.ssh
    #sudo echo "${LAPPUBLICKEY}" | sudo tee /root/.ssh/authorized_keys

    mkdir -p /home/"$USER"/.ssh
    #chown "$USER" "/home/${USER}/.ssh"
    #echo "${LAPPUBLICKEY}" >> /home/"$USER"/.ssh/authorized_keys

    pushd /home/"${USER}"/.ssh/
    [ -e id_rsa ] || ssh-keygen -t rsa -N "" -f id_rsa
    # TODO: Get this working: ssh-copy-id -i id_rsa.pub "${LAP_IP}"
    popd
}

function install-gpu-drivers {
    # These are needed for xmr-stak, but I think at least one needs to be installed before amdgpu-pro is installed
    sudo apt install -y build-essential cmake libhwloc-dev libmicrohttpd-dev libssl-dev ocl-icd-opencl-dev
    mkdir -p /home/"${USER}"/drivers
    pushd /home/"${USER}"/drivers

    if [ ! -e drivers.md5 ] || ! md5sum -c drivers.md5 ]
    then
        echo "Copying drivers from laptop"
        scp keith@"${LAP_IP}":~keith/Documents/amdgpu-pro-17.50-511655.tar.xz .
        md5sum amdgpu-pro-17.50-511655.tar.xz > drivers.md5
    else
        echo "Drivers tar already here, not copying."
    fi

    tar -Jxf amdgpu-pro-17.50-511655.tar.xz
    cd amdgpu-pro-17.50-511655
    ./amdgpu-pro-install -y --headless --opencl=legacy
    sudo adduser "$USER" video
    sudo adduser root video

    popd
}


function install-graphite {
    # https://www.digitalocean.com/community/tutorials/how-to-install-and-use-graphite-on-an-ubuntu-14-04-server
    #https://bugs.launchpad.net/ubuntu/+source/graphite-web/+bug/1648828/comments/2
    sudo apt-get install -y graphite-web graphite-carbon
    sudo apt-get install -y postgresql libpq-dev python-psycopg2

    userCnt=$(sudo -u postgres psql -tAc "SELECT count(*) FROM pg_user WHERE usename = 'graphite';")
    if [ $((userCnt > 0)) ]
    then
        sudo -u postgres psql -c "CREATE USER graphite WITH PASSWORD '${GRAPHITE_PW}';"
        sudo -u postgres psql -c "CREATE DATABASE graphite WITH OWNER graphite;"
    fi

    #sudo sed -ri "s/^#?(SECRET_KEY = ).*/\1'${GRAHPITE_KEY}'/"
    #sudo sed -ri "s/^#?(TIME_ZONE = ).*/\1'America/Denver'/"
    #sudo sed -ri "s/^#?(USE_REMOTE_USER_AUTHENTICATION = *)/\1/"

    pushd /etc/graphite
    sudo sed -ri "s/^#?(SECRET_KEY = ).*/\1'${GRAHPITE_KEY}'/" local_settings.py
    sudo sed -ri "s/^#?(TIME_ZONE = ).*/\1'America\/Denver'/" local_settings.py
    sudo sed -ri "s/^#?(USE_REMOTE_USER_AUTHENTICATION = ).*/\1True/" local_settings.py
    sudo sed -ri "s/(^\s+'NAME': ).*/\1'graphite',/" local_settings.py
    sudo sed -ri "s/(^\s+'ENGINE': ).*/\1'django.db.backends.postgresql_psycopg2',/" local_settings.py
    sudo sed -ri "s/(^\s+'USER': ).*/\1'graphite',/" local_settings.py
    sudo sed -ri "s/(^\s+'PASSWORD': ).*/\1'${GRAPHITE_PW}',/" local_settings.py
    sudo sed -ri "s/(^\s+'?HOST': ).*/\1'127.0.0.1',/" local_settings.py
    popd

    # Initiliaze graphite DB
    sudo graphite-manage migrate auth
    sudo graphite-manage syncdb

    # Start carbon service on boot
    sudo sed -ri "s/(^CARBON_CACHE_ENABLED=).*/\1true/" /etc/default/graphite-carbon

    # Should I just let logroate handle log rotaton?
    sudo sed -ri "s/(ENABLE_LOGROTATION = ).*/\1True/" /etc/carbon/carbon.conf

    # Configure carbon retention policy
    sudo sed -ri 's/(retentions =) 60s:1d/\1 60s:7d,1h:14d,1d:4w,1w:2y/' /etc/carbon/storage-schemas.conf
    sudo systemctl start carbon-cache

    # Install Apache
    sudo apt-get install -y apache2 libapache2-mod-wsgi
    sudo a2dissite 000-default
    sudo cp /usr/share/graphite-web/apache2-graphite.conf /etc/apache2/sites-available
    sudo a2ensite apache2-graphite
    sudo service apache2 reload

}

function make-ip-static {
    pushd /etc/network/

    sudo sed -rn 's/^#?(iface enp5s0 inet .*)/#\1/p' interfaces
    sudo sed -i '/# BEGIN CUSTOM/,/# END CUSTOM/d' interfaces
    sudo cat <<EOF | sudo tee -a interfaces
# BEGIN CUSTOM
iface enp5s0 inet static
    address "${HAF_IP}"
    netmask "${HAF_NETWORK}"
    network "${HAF_NETWORK}"
    broadcast "${HAF_BROADCAST}"
    gatewat "${HAF_GATEWAY}"
# END CUSTOM
EOF
}

function set-hostname {
    echo "HAF" | sudo tee /etc/hostname
    sudo sed -ri's/(^127\.0\.1\.1\s+).*/\1 HAF/' /etc/hosts
    sudo hostname HAF
}

set -x
set -e

source secrets.sh
[ -n "${LAPPUBLICKEY}" ] || echo "****** LAPPUBLICKEY is empty! *****"
USER=$(whoami)

APTUPDATE=1
#GPUDRIVERS=0
while (( $# )) && [[ "$1" == --* ]]
do
    case $1 in 
        --update-os ) shift; sudo apt upgrade -y;;
        --no-apt-update ) shift; APTUPDATE=0;;
#        --gpu-drivers ) shift; GPUDRIVERS=1;;
    esac
    shift
done

source install_xmr-stak.sh

if (( APTUPDATE ))
then
    sudo apt-get update
    sudo apt autoremove -y
fi

if [ -n "$(which amdgpu-pro-uninstall)" ]
then
    echo "amdgpu-pro drivers already installed."
    if (( GPUDRIVERS ))
    then
        echo "Forcing reinstall."
        install-gpu-drivers
    fi
else
    echo "amdgpu-pro drivers are not installed yet, installing now."
    install-gpu-drivers
fi


customize_bash
setup-ssh
setup_squid
install-fail2ban
install-swapspace
install-xmr-stak
install-graphite


echo "ALL DONE!"
