#!/bin/bash

function install-xmr-stak {
    sudo apt install -y build-essential cmake libhwloc-dev libmicrohttpd-dev libssl-dev ocl-icd-opencl-dev
    # Do I need libpci-dev?
    rundir="$(pwd)"
    pushd /opt

    # Download and build xmr-stak
    [ -d xmr-stak ] || sudo git clone https://github.com/fireice-uk/xmr-stak.git xmr-stak
    sudo chown -R "$USER:" xmr-stak
    mkdir -p xmr-stak/build
    pushdDir=$(pwd)
    cd xmr-stak/build
    cmake .. -DCUDA_ENABLE=OFF -DOpenCL_LIBRARY=/opt/amdgpu-pro/lib/x86_64-linux-gnu/libOpenCL.so -DOpenCL_INCLUDE_DIR=/usr/include/
    make
    cd "$pushdDir"

    # xmr-stak config file
    if [ -e "${rundir}/config.txt" ]
    then
        cp "${rundir}/config.txt" xmr-stak/config.txt
    else
        cat <<EOF
****************************
Missing xmr-stak config.txt!
****************************
EOF
    fi

    # Script that systemd will start to start Monero mining
    cat <<EOF > xmr-stak/start_mining.sh
#!/bin/bash
export GPU_FORCE_64BIT_PTR=1
export GPU_USE_SYNC_OBJECTS=1
export GPU_MAX_ALLOC_PERCENT=100
export GPU_SINGLE_ALLOC_PERCENT=100
export GPU_MAX_HEAP_SIZE=100
sudo sysctl -w vm.nr_hugepages=128
/opt/xmr-stak/build/bin/xmr-stak -c /opt/xmr-stak/config.txt
EOF

    chmod +x xmr-stak/start_mining.sh

    # Setup script that sends mining stats to Graphite
    sudo apt install -y python-requests
    if [ -e "${rundir}/hashrate_poller.py" ]
    then
        cp "${rundir}/hashrate_poller.py" /opt/xmr-stak/
    else
        pwd=$(pwd)
        cat <<EOF
****************************
Missing hashrate_poller.py!
****************************
pwd=$pwd
EOF
    fi

    popd


    # TODO: Do I need to edit /etc/OpenCL/vendor/amdocl64.icd to have full path?


    # Change memory settings to allow xmr-stak to use more memory
    sudo sed -i '/# BEGIN CUSTOM/,/# END CUSTOM/d' /etc/security/limits.conf
    sudo cat <<EOF | sudo tee -a /etc/security/limits.conf
# BEGIN CUSTOM
    vm.nr_hugepages=128
    * soft memlock 262144
    * hard memlock 262144
# END CUSTOM
EOF

    # Rotate log file, this assumes the confg.txt file put the logfile
    # in /var/log/xmr-stak.log.
    # TODO: if config.txt exists, parse it for log file location.
    cat <<EOF | sudo tee /etc/logrotate.d/xmr-stak
/var/log/xmr-stak.log {
    compress
    missingok
    notifempty
}
EOF

    # Use systemd to start maining
    cat <<EOF | sudo tee /etc/systemd/system/xmr-stak.service
[Unit]
Description=Monero mining
WorkingDirectory=/opt/xmr-stak

[Service]
ExecStart=/opt/xmr-stak/start_mining.sh

[Install]
WantedBy = multi-user.target
EOF

    # Start mining
    sudo systemctl start xmr-stak


    # Use systemd to start script to send mining stats to Graphite
    cat <<EOF | sudo tee /etc/systemd/system/xmr-stak_graphite.service
[Unit]
Description=Send Monero mining stats to Graphite
WorkingDirectory=/opt/xmr-stak

[Service]
ExecStart=/opt/xmr-stak/hashrate_poller.py > /dev/null

[Install]
WantedBy = multi-user.target
EOF

    sudo systemctl start xmr-stak_graphite

echo "Finished installin xmr-stak service."
}

